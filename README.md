# RDS Postgres wrapper module for Teamcity project

This module is wrapper for [terraform-aws-rds](https://github.com/terraform-aws-modules/terraform-aws-rds) module

The module creates:
  - RDS Postgres Database
  - Subnet group for the RDS
  - Parameter group of the RDS
  - Security group for access to the RDS
  - SSM parameters for storing DB username and password

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| allocated\_storage | The allocated storage in gigabytes | `string` | `20` | no |
| availability\_zone | The Availability Zone of the master RDS instance | `string` | `""` | no |
| backup\_retention\_period | The days to retain backups for. Value 0 disable backups. Must be greater than 0 when replication enabled | `number` | `0` | no |
| backup\_window | The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance\_window | `string` | `"02:00-04:00"` | no |
| db\_name | The DB name to create. If omitted, no database is created initially | `string` | `""` | no |
| deletion\_protection | The database can't be deleted when this value is set to true | `bool` | `false` | no |
| engine\_version | The engine version to use | `string` | n/a | yes |
| final\_snapshot\_identifier | The name of your final DB snapshot when this DB instance is deleted | `string` | n/a | yes |
| instance\_class | The instance type of the RDS instance | `string` | `"db.t2.small"` | no |
| maintenance\_window | The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00' | `string` | `"Mon:00:00-Mon:02:00"` | no |
| major\_engine\_version | Specifies the major version of the engine that this option group should be associated with | `string` | `""` | no |
| master\_db\_password | Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file | `string` | n/a | yes |
| master\_db\_username | Username for the master DB user | `string` | n/a | yes |
| pg\_family | The family of the DB parameter group | `string` | n/a | yes |
| pg\_parameters | A list of DB parameter maps to apply | `list(map(string))` | `[]` | no |
| pg\_port | The port on which the DB accepts connections | `string` | `"5432"` | no |
| rds\_whitelist\_cidr | Source CIDR list allowed for RDS connect | `string` | `"0.0.0.0/0"` | no |
| region | AWS region | `string` | n/a | yes |
| skip\_final\_snapshot | Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final\_snapshot\_identifier | `bool` | `true` | no |
| ssm\_key\_id | The KMS key id or arn for encrypting a SecureString parameter in SSM | `string` | `""` | no |
| ssm\_paramter\_prefix | The prefix for parameters name stored in SSM | `string` | `"/"` | no |
| storage\_encrypted | Specifies whether the DB instance is encrypted | `bool` | `false` | no |
| storage\_type | One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'standard' if not. Note that this behaviour is different from the AWS web console, where the default is 'gp2'. | `string` | `"gp2"` | no |
| subnet\_ids | A list of VPC subnet IDs | `list(string)` | n/a | yes |
| tags | A mapping of tags to assign to all resources | `map(string)` | `{}` | no |
| use\_ssm\_parameter | Store the db username and password as a SecureString parameters in SSM | `bool` | `true` | no |
| vpc\_id | The VPC for RDS SG creating | `string` | n/a | yes |
| vpc\_security\_group\_ids | List of VPC security groups to associate | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| postgres\_db\_instance\_address | The address of the (master, if replication enabled) RDS instance |
| postgres\_db\_instance\_availability\_zone | The availability zone of the (master, if replication enabled) RDS instance |
| postgres\_db\_instance\_endpoint | The (master, if replication enabled) connection endpoint |
| postgres\_db\_instance\_name | The database name |
| postgres\_db\_instance\_port | The (master, if replication enabled) database port |
| rds\_password\_ssm\_parameter\_arn | ARN of DB password parameter stored in SSM |
| rds\_password\_ssm\_parameter\_name | DB password parameter name (not value) stored in SSM |
| rds\_security\_group\_id | RDS Security Group ID |
| rds\_security\_group\_name | RDS Security Group name |
| rds\_username\_ssm\_parameter\_arn | ARN of DB username parameter stored in SSM |
| rds\_username\_ssm\_parameter\_name | DB username parameter name (not value) stored in SSM |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
