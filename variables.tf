variable "region" {
  description = "AWS region"
  type        = string
}

variable "engine_version" {
  description = "The engine version to use"
  type        = string
}

variable "major_engine_version" {
  description = "Specifies the major version of the engine that this option group should be associated with"
  type        = string
  default     = ""
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
  default     = "db.t2.small"
}

variable "allocated_storage" {
  description = "The allocated storage in gigabytes"
  type        = string
  default     = 20
}

variable "storage_type" {
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'standard' if not. Note that this behaviour is different from the AWS web console, where the default is 'gp2'."
  type        = string
  default     = "gp2"
}

variable "storage_encrypted" {
  description = "Specifies whether the DB instance is encrypted"
  type        = bool
  default     = false
}

variable "db_name" {
  description = "The DB name to create. If omitted, no database is created initially"
  type        = string
  default     = ""
}

variable "master_db_username" {
  description = "Username for the master DB user"
  type        = string
}

variable "master_db_password" {
  description = "Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file"
  type        = string
}

variable "pg_port" {
  description = "The port on which the DB accepts connections"
  type        = string
  default     = "5432"
}

variable "vpc_security_group_ids" {
  description = "List of VPC security groups to associate"
  type        = list(string)
  default     = []
}

variable "maintenance_window" {
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'"
  type        = string
  default     = "Mon:00:00-Mon:02:00"
}

variable "backup_window" {
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance_window"
  type        = string
  default     = "02:00-04:00"
}

variable "backup_retention_period" {
  description = "The days to retain backups for. Value 0 disable backups. Must be greater than 0 when replication enabled"
  type        = number
  default     = 0
}

variable "final_snapshot_identifier" {
  description = "The name of your final DB snapshot when this DB instance is deleted"
  type        = string
  default     = null
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
  type        = bool
  default     = true
}

variable "subnet_ids" {
  description = "A list of VPC subnet IDs"
  type        = list(string)
}

variable "pg_family" {
  description = "The family of the DB parameter group"
  type        = string
}

variable "pg_parameters" {
  description = "A list of DB parameter maps to apply"
  type        = list(map(string))
  default     = []
}

variable "availability_zone" {
  description = "The Availability Zone of the master RDS instance"
  type        = string
  default     = ""
}

variable "deletion_protection" {
  description = "The database can't be deleted when this value is set to true"
  type        = bool
  default     = false
}

variable "vpc_id" {
  description = "The VPC for RDS SG creating"
  type        = string
}

variable "rds_whitelist_cidr" {
  description = "Source CIDR list allowed for RDS connect"
  type        = string
  default     = "0.0.0.0/0"
}

variable "ssm_key_id" {
  description = "The KMS key id or arn for encrypting a SecureString parameter in SSM"
  type        = string
  default     = ""
}

variable "use_ssm_parameter" {
  description = "Store the db username and password as a SecureString parameters in SSM"
  type        = bool
  default     = true
}

variable "ssm_paramter_prefix" {
  description = "The prefix for parameters name stored in SSM"
  type        = string
  default     = "/"
}

variable "tags" {
  description = "A mapping of tags to assign to all resources"
  type        = map(string)
  default     = {}
}
