terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.20"
  }
}

provider "aws" {
  region = var.region
}

module "rds-postgres" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  engine = "postgres"

  subnet_ids = var.subnet_ids

  family     = var.pg_family
  parameters = var.pg_parameters

  major_engine_version = var.major_engine_version
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  allocated_storage    = var.allocated_storage
  storage_type         = var.storage_type
  storage_encrypted    = var.storage_encrypted

  identifier = var.db_name
  name       = var.db_name
  username   = var.master_db_username
  password   = var.master_db_password
  port       = var.pg_port

  vpc_security_group_ids = concat(var.vpc_security_group_ids, [module.sg_rds.this_security_group_id])

  availability_zone = var.availability_zone

  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window

  maintenance_window = var.maintenance_window

  skip_final_snapshot       = var.skip_final_snapshot
  final_snapshot_identifier = var.final_snapshot_identifier

  deletion_protection = var.deletion_protection

  tags = var.tags
}

module "sg_rds" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "${var.db_name}-db-sg"
  description = "Access to Postgres RDS"
  vpc_id      = var.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = var.pg_port
      to_port     = var.pg_port
      protocol    = "tcp"
      description = "Access to Postgres RDS"
      cidr_blocks = var.rds_whitelist_cidr
    },
  ]

  tags = var.tags
}

module "ssm_db_username" {
  source  = "tmknom/ssm-parameter/aws"
  version = "2.0.0"

  name  = "${var.ssm_paramter_prefix}${var.db_name}/master_username"
  value = var.master_db_username

  enabled = var.use_ssm_parameter

  description = "RDS master username"
  key_id      = var.ssm_key_id
  overwrite   = true

  # TODO: Use strong regexp
  allowed_pattern = ".+"

  tags = var.tags
}

module "ssm_db_password" {
  source  = "tmknom/ssm-parameter/aws"
  version = "2.0.0"

  name  = "${var.ssm_paramter_prefix}${var.db_name}/master_password"
  value = var.master_db_password

  enabled = var.use_ssm_parameter

  description = "RDS master password"
  key_id      = var.ssm_key_id
  overwrite   = true

  # TODO: Use strong regexp
  allowed_pattern = ".+"

  tags = var.tags
}
