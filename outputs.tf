output "postgres_db_instance_address" {
  description = "The address of the (master, if replication enabled) RDS instance"
  value       = module.rds-postgres.this_db_instance_address
}

output "postgres_db_instance_endpoint" {
  description = "The (master, if replication enabled) connection endpoint"
  value       = module.rds-postgres.this_db_instance_endpoint
}

output "postgres_db_instance_port" {
  description = "The (master, if replication enabled) database port"
  value       = module.rds-postgres.this_db_instance_port
}

output "postgres_db_instance_availability_zone" {
  description = "The availability zone of the (master, if replication enabled) RDS instance"
  value       = module.rds-postgres.this_db_instance_availability_zone
}

output "postgres_db_instance_name" {
  description = "The database name"
  value       = module.rds-postgres.this_db_instance_name
}

output "rds_security_group_id" {
  value       = module.sg_rds.this_security_group_id
  description = "RDS Security Group ID"
}

output "rds_security_group_name" {
  value       = module.sg_rds.this_security_group_name
  description = "RDS Security Group name"
}

output "rds_username_ssm_parameter_name" {
  value       = module.ssm_db_username.ssm_parameter_name
  description = "DB username parameter name (not value) stored in SSM"
}

output "rds_password_ssm_parameter_name" {
  value       = module.ssm_db_password.ssm_parameter_name
  description = "DB password parameter name (not value) stored in SSM"
}

output "rds_username_ssm_parameter_arn" {
  value       = module.ssm_db_username.ssm_parameter_arn
  description = "ARN of DB username parameter stored in SSM"
}

output "rds_password_ssm_parameter_arn" {
  value       = module.ssm_db_password.ssm_parameter_arn
  description = "ARN of DB password parameter stored in SSM"
}
